﻿using System.Linq;
using SampleApplication.Core.Commands;
using SampleApplication.Core.Domain;
using SampleApplication.Core.Infrastructure.Commands;
using SampleApplication.Core.Infrastructure.Storage;
using SampleApplication.DataContracts;

namespace SampleApplication.Core.CommandHandlers
{
	/// <summary>
	/// Handles <see cref="GetCustomerCommand"/>
	/// </summary>
	internal class GetCustomerCommandHandler : CommandHandlerBase<GetCustomerCommand, CustomerDto>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="GetCustomerCommandHandler"/> class
		/// </summary>
		public GetCustomerCommandHandler(IRepository repository)
			: base(repository)
		{ }

		/// <inheritdoc/>
		protected override CommandResult<CustomerDto> HandleInternal(GetCustomerCommand command)
		{
			var customer = this.Repository.Query<Customer>().Where(c => c.Id == command.Id).SingleOrDefault();
			if (customer == null)
			{
				return CommandResult<CustomerDto>.Failed("Customer not found");
			}

			var orders = this.Repository.Query<Order>().Where(o => o.Customer == customer).ToList();
			var customerDto = new CustomerDto
			{
				Id = customer.Id,
				Email = customer.Email,
				Name = customer.Name,
				Orders = orders.Select(o => new OrderDto { Id = o.Id, Price = o.Price, CreationDate = o.CreationDate }).ToList()
			};

			return CommandResult<CustomerDto>.Ok(customerDto);
		}
	}

}
