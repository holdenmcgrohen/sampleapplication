﻿using System.Linq;
using SampleApplication.Core.Commands;
using SampleApplication.Core.Domain;
using SampleApplication.Core.Infrastructure.Commands;
using SampleApplication.Core.Infrastructure.Storage;
using SampleApplication.DataContracts;

namespace SampleApplication.Core.CommandHandlers
{
	/// <summary>
	/// Handles <see cref="GetCustomersCommand"/>
	/// </summary>
	internal class GetCustomersCommandHandler : CommandHandlerBase<GetCustomersCommand, CustomersDataWrapperDto>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="GetCustomersCommandHandler"/> class
		/// </summary>
		public GetCustomersCommandHandler(IRepository repository)
			: base(repository)
		{ }

		/// <inheritdoc/>
		protected override CommandResult<CustomersDataWrapperDto> HandleInternal(GetCustomersCommand command)
		{
			var dtoList = this.Repository
				.Query<Customer>()
				.Select(p => new CustomerDto { Id = p.Id, Name = p.Name, Email = p.Email })
				.ToList();

			return CommandResult<CustomersDataWrapperDto>.Ok(new CustomersDataWrapperDto { Customers = dtoList });
		}
	}
}
