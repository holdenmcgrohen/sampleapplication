﻿using System.Linq;
using SampleApplication.Core.Commands;
using SampleApplication.Core.Domain;
using SampleApplication.Core.Infrastructure.Commands;
using SampleApplication.Core.Infrastructure.Storage;
using SampleApplication.DataContracts;

namespace SampleApplication.Core.CommandHandlers
{
	/// <summary>
	/// Handles <see cref="CreateCustomerCommand"/>
	/// </summary>
	internal class CreateCustomerCommandHandler : CommandHandlerBase<CreateCustomerCommand, CustomerDto>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="CreateCustomerCommandHandler"/> class
		/// </summary>
		public CreateCustomerCommandHandler(IRepository repository)
			: base(repository)
		{ }

		/// <inheritdoc/>
		protected override CommandResult<CustomerDto> HandleInternal(CreateCustomerCommand command)
		{
			var customer = new Customer
			{
				Email = command.Email,
				Name = command.Name,
			};

			if (this.Repository.Query<Customer>().Where(c => c.Email == customer.Email).Any())
			{
				return CommandResult<CustomerDto>.Failed("Email already registered");
			}

			this.Repository.Save(customer);

			var customerDto = new CustomerDto
			{
				Id = customer.Id,
				Email = customer.Email,
				Name = customer.Name,
				Orders = new OrderDto[0]
			};

			return CommandResult<CustomerDto>.Ok(customerDto);
		}
	}
}
