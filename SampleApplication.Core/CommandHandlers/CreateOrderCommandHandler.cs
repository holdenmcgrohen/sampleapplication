﻿using System.Linq;
using SampleApplication.Core.Commands;
using SampleApplication.Core.Domain;
using SampleApplication.Core.Infrastructure.Commands;
using SampleApplication.Core.Infrastructure.Storage;
using SampleApplication.DataContracts;

namespace SampleApplication.Core.CommandHandlers
{
	/// <summary>
	/// Handles <see cref="CreateOrderCommand"/>
	/// </summary>
	internal class CreateOrderCommandHandler : CommandHandlerBase<CreateOrderCommand, OrderDto>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="CreateOrderCommandHandler"/> class
		/// </summary>
		public CreateOrderCommandHandler(IRepository repository)
			: base(repository)
		{ }

		/// <inheritdoc/>
		protected override CommandResult<OrderDto> HandleInternal(CreateOrderCommand command)
		{
			var customer = this.Repository.Query<Customer>().Where(c => c.Id == command.CustomerId).SingleOrDefault();

			if (customer == null)
			{
				return CommandResult<OrderDto>.Failed("Customer not found");
			}

			var order = new Order
			{
				Customer = customer,
				Price = command.Price.Value
			};

			this.Repository.Save(order);

			var orderDto = new OrderDto
			{
				Id = order.Id,
				Price = order.Price,
				CreationDate = order.CreationDate,
			};

			return CommandResult<OrderDto>.Ok(orderDto);
		}
	}
}
