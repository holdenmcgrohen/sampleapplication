﻿namespace SampleApplication.Core.Infrastructure.Commands
{
	/// <summary>
	/// Contains logic to find and launch command handlers
	/// </summary>
	public static class CommandProcessor
	{
		/// <summary>
		/// Processes the command and returns a result
		/// </summary>
		/// <typeparam name="TCommand">Type of the command to be processed</typeparam>
		/// <param name="command">Command to be processed</param>
		/// <returns>Command result</returns>
		public static CommandResult<TResultData> Process<TCommand, TResultData>(TCommand command)
			where TCommand : CommandBase<TResultData>
		{
			var handler = ServiceLocator.GetInstance<CommandHandlerBase<TCommand, TResultData>>();
			return handler.Handle(command);
		}
	}
}
