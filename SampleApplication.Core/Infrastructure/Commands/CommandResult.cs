﻿using System;
using System.Collections.Generic;
using SampleApplication.DataContracts;

namespace SampleApplication.Core.Infrastructure.Commands
{
	/// <summary>
	/// Contains information about a command execution result
	/// </summary>
	public class CommandResult<TResultData> : ApiResponse<TResultData>
	{
		/// <summary>
		/// Builds a successful command result
		/// </summary>
		/// <param name="data">Data</param>
		/// <returns>The command result</returns>
		public static CommandResult<TResultData> Ok(TResultData data)
		{
			return new CommandResult<TResultData> { Success = true, Data = data };
		}

		/// <summary>
		/// Builds an unsuccessful command result
		/// </summary>
		/// <param name="errorMessage">The error message</param>
		/// <returns>The command result</returns>
		public static CommandResult<TResultData> Failed(string errorMessage)
		{
			return new CommandResult<TResultData> { Success = false, Errors = new[] { errorMessage } };
		}

		/// <summary>
		/// Builds an unsuccessful command result
		/// </summary>
		/// <param name="errorMessages">The error messages</param>
		/// <returns>The command result</returns>
		public static CommandResult<TResultData> Failed(ICollection<string> errorMessages)
		{
			if (errorMessages == null)
			{
				throw new ArgumentNullException(nameof(errorMessages));
			}

			return new CommandResult<TResultData> { Success = false, Errors = errorMessages };
		}
	}
}
