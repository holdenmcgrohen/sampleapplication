﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using SampleApplication.Core.Infrastructure.Storage;

namespace SampleApplication.Core.Infrastructure.Commands
{
	/// <summary>
	/// Abstract handler that can handle <see cref="TCommand"/> and produce <see cref="TResultData"/>
	/// </summary>
	/// <typeparam name="TCommand">The type of command that the current handler can handle</typeparam>
	/// <typeparam name="TResultData">The type of the data that should be produced as the result of command processing</typeparam>
	internal abstract class CommandHandlerBase<TCommand, TResultData>
		where TCommand : CommandBase<TResultData>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="CommandHandlerBase<TCommand>"/> class
		/// </summary>
		public CommandHandlerBase(IRepository repository)
		{
			if (repository == null)
			{
				throw new ArgumentNullException(nameof(repository));
			}

			this.Repository = repository;
		}

		/// <summary>
		/// Repository
		/// </summary>
		protected IRepository Repository { get; }

		/// <summary>
		/// Handles the provided command
		/// </summary>
		/// <param name="command">The command to handle</param>
		/// <returns>The result</returns>
		public CommandResult<TResultData> Handle(TCommand command)
		{
			if (command == null)
			{
				throw new ArgumentNullException(nameof(command));
			}

			var validationResults = new List<ValidationResult>();
			var commandIsValid = Validator.TryValidateObject(command, new ValidationContext(command), validationResults, true);
			if (!commandIsValid)
			{
				return CommandResult<TResultData>.Failed(validationResults.Select(r => r.ErrorMessage).ToList());
			}

			return this.HandleInternal(command);
		}

		protected abstract CommandResult<TResultData> HandleInternal(TCommand command);
	}
}
