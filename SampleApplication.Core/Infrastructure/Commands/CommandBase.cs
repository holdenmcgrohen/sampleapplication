﻿namespace SampleApplication.Core.Infrastructure.Commands
{
	/// <summary>
	/// Represents a command that can be processed using <see cref="CommandProcessor"/>
	/// </summary>
	/// <typeparam name="TResultData">The type of the data that should be returned as the result of command processing</typeparam>
	public abstract class CommandBase<TResultData>
	{
	}
}
