﻿using System;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Castle.Windsor.Configuration.Interpreters;
using SampleApplication.Core.Infrastructure.Commands;

namespace SampleApplication.Core.Infrastructure
{
	/// <summary>
	/// ServiceLocator
	/// </summary>
	public static class ServiceLocator
	{
		private static IWindsorContainer Container;

		static ServiceLocator()
		{
			Container = new WindsorContainer(new XmlInterpreter());
			InitializeCommonServices();
		}

		/// <summary>
		/// Gets an instance of a service
		/// </summary>
		/// <typeparam name="T">Service type</typeparam>
		/// <returns>Service instance</returns>
		public static T GetInstance<T>()
		{
			return Container.Resolve<T>();
		}

		internal static void Register<TService>(TService instance)
			where TService : class
		{
			Container.Register(
				Component.For<TService>()
					.Instance(instance)
					.IsDefault()
					.Named($"{typeof(TService).Name}_{Guid.NewGuid()}")
					.LifestyleSingleton());
		}

		private static void InitializeCommonServices()
		{
			Container.Register(
				Classes.FromThisAssembly()
					.IncludeNonPublicTypes()
					.BasedOn(typeof(CommandHandlerBase<,>))
					.WithService.Base().LifestyleTransient());
		}
	}
}
