﻿using System;
using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions.Helpers;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using SampleApplication.Core.Domain;

namespace SampleApplication.Core.Infrastructure.Storage
{
	internal class NHibernateSqLiteRepository
	{
		private static object SyncLock = new object();

		private static ISessionFactory SessionFactory;
		
		public NHibernateSqLiteRepository(string connectionString)
		{
			var automappingConfiguration = new AutomappingConfiguration();

			lock (SyncLock)
			{
				if (SessionFactory == null)
				{
					SessionFactory = Fluently.Configure()
					.Database(SQLiteConfiguration.Standard.ConnectionString(connectionString))
					.Mappings(m =>
					  m.AutoMappings
						.Add(
						  AutoMap.AssemblyOf<NHibernateSqLiteRepository>(automappingConfiguration)
							  .Conventions.Add(PrimaryKey.Name.Is(x => "Id"), ForeignKey.EndsWith("Id"))
							  .Override<Entity>(map => map.Id(x => x.Id).Not.Nullable())
							  .Override<Entity>(map => map.Map(x => x.CreationDate).Not.Nullable())
							  .Override<Customer>(map => map.Map(x => x.Name).Not.Nullable().Length(500))
							  .Override<Customer>(map => map.Map(x => x.Email).Not.Nullable().Length(500).Unique())
							  .Override<Order>(map => map.References(x => x.Customer).Not.Nullable())
						)
					  )
					  .ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(false, true))
					.BuildSessionFactory();
				}
			}
		}

		public ISession OpenSession()
		{
			return SessionFactory.OpenSession();
		}

		class AutomappingConfiguration : DefaultAutomappingConfiguration
		{
			public override bool ShouldMap(Type type)
			{
				return typeof(Entity).IsAssignableFrom(type) && !type.IsAbstract;
			}
		}
	}	
}