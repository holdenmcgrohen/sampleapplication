﻿using System;
using System.Linq;
using NHibernate;
using NHibernate.Linq;
using SampleApplication.Core.Domain;

namespace SampleApplication.Core.Infrastructure.Storage
{
	internal class SqlRepository : IRepository
	{
		private readonly ISession session;

		public SqlRepository(string connectionString)
		{
			if (connectionString == null)
			{
				throw new ArgumentNullException(nameof(connectionString));
			}

			var internalRepository = new NHibernateSqLiteRepository(connectionString);
			this.session = internalRepository.OpenSession();
		}

		public IQueryable<TEntity> Query<TEntity>()
			where TEntity : Entity
		{
			return this.session.Query<TEntity>();
		}

		public void Save<TEntity>(TEntity entity)
			where TEntity : Entity
		{
			using (var transaction = this.session.BeginTransaction())
			{
				if (entity.Id == 0)
				{
					entity.CreationDate = DateTime.UtcNow;
				}

				this.session.SaveOrUpdate(entity);
				transaction.Commit();
			}
		}
	}
}
