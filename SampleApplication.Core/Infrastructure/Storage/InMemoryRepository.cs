﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SampleApplication.Core.Domain;

namespace SampleApplication.Core.Infrastructure.Storage
{
	/// <summary>
	/// Repository that holds data in memory
	/// </summary>
	internal class InMemoryRepository : IRepository
	{
		private static readonly object SyncLock = new object();

		private readonly Dictionary<Type, ICollection> data = new Dictionary<Type, ICollection>();

		/// <summary>
		/// Initializes a new instance of the <see cref="InMemoryRepository"/> class
		/// </summary>
		public InMemoryRepository()
			: this(true)
		{ }

		internal InMemoryRepository(bool preLoadSampleData)
		{
			if (preLoadSampleData)
			{
				this.LoadSampleData();
			}
		}

		/// <inheritdoc/>
		public IQueryable<TEntity> Query<TEntity>()
			where TEntity : Entity
		{
			var list = GetOrCreateList<TEntity>();
			return list.AsQueryable();
		}

		/// <inheritdoc/>
		public void Save<TEntity>(TEntity entity)
			where TEntity : Entity
		{
			var list = GetOrCreateList<TEntity>();
			if (entity.Id == 0)
			{
				entity.Id = list.Count + 1;
				entity.CreationDate = DateTime.UtcNow;
				list.Add(entity);
			}
		}

		private List<TEntity> GetOrCreateList<TEntity>()
		{
			lock (SyncLock)
			{
				if (this.data.ContainsKey(typeof(TEntity)))
				{
					return (List<TEntity>)this.data[typeof(TEntity)];
				}
				else
				{
					var list = new List<TEntity>();
					this.data[typeof(TEntity)] = list;
					return list;
				}
			}
		}

		private void LoadSampleData()
		{
			var customer1 = new Customer { Name = "Jim", Email = "jim@me.com" };
			this.Save(customer1);

			var customer2 = new Customer { Name = "Bob", Email = "bob@me.com" };
			this.Save(customer2);
		}
	}

}
