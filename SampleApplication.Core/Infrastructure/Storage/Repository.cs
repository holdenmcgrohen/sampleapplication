﻿using System.Linq;
using SampleApplication.Core.Domain;

namespace SampleApplication.Core.Infrastructure.Storage
{
	/// <summary>
	/// Repository for data access
	/// </summary>
	public interface IRepository
	{
		/// <summary>
		/// Performs a data query
		/// </summary>
		/// <typeparam name="TEntity">Type of entity to query</typeparam>
		/// <returns>The query</returns>
		IQueryable<TEntity> Query<TEntity>() where TEntity : Entity;

		/// <summary>
		/// Saves the entity into the storage
		/// </summary>
		/// <typeparam name="TEntity">Type of entity to save</typeparam>
		/// <param name="entity">Entity to save</param>
		void Save<TEntity>(TEntity entity) where TEntity : Entity;
	}
}
