﻿using System;

namespace SampleApplication.Core.Domain
{
	/// <summary>
	/// Base class for all domain entities
	/// </summary>
	public abstract class Entity
	{
		public Entity()
		{
			this.CreationDate = DateTime.UtcNow;
		}

		/// <summary>
		/// Unique identifier
		/// </summary>
		public virtual int Id { get; set; }

		/// <summary>
		/// Creation date and time
		/// </summary>
		public virtual DateTime CreationDate { get; set; }
	}
}
