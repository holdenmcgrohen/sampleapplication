﻿namespace SampleApplication.Core.Domain
{
	/// <summary>
	/// Customer's order
	/// </summary>
	public class Order : Entity
	{
		/// <summary>
		/// Total price
		/// </summary>
		public virtual decimal Price { get; set; }

		/// <summary>
		/// Customer
		/// </summary>
		public virtual Customer Customer { get; set; } 
	}
}
