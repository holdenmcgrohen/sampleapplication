﻿namespace SampleApplication.Core.Domain
{
	/// <summary>
	/// Customer capable of placing orders
	/// </summary>
	public class Customer : Entity
	{
		/// <summary>
		/// Name
		/// </summary>
		public virtual string Name { get; set; }

		/// <summary>
		/// Email address
		/// </summary>
		public virtual string Email { get; set; }
	}
}
