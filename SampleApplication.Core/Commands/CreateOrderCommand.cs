﻿using System.ComponentModel.DataAnnotations;
using SampleApplication.Core.Infrastructure.Commands;
using SampleApplication.DataContracts;

namespace SampleApplication.Core.Commands
{
	/// <summary>
	/// A command to add an order to an existing customer
	/// </summary>
	public class CreateOrderCommand : CommandBase<OrderDto>
	{
		/// <summary>
		/// Customer identifier
		/// </summary>
		[Required]
		public int? CustomerId { get; set; }

		/// <summary>
		/// Order price
		/// </summary>
		[Required]
		[Range(0, 100000)]
		public decimal? Price { get; set; }
	}
}
