﻿using SampleApplication.Core.Infrastructure.Commands;
using SampleApplication.DataContracts;

namespace SampleApplication.Core.Commands
{
	/// <summary>
	/// A command to retrieve all customers' data
	/// </summary>
	public class GetCustomersCommand : CommandBase<CustomersDataWrapperDto>
	{
	}
}
