﻿using System.ComponentModel.DataAnnotations;
using SampleApplication.Core.Infrastructure.Commands;
using SampleApplication.DataContracts;

namespace SampleApplication.Core.Commands
{
	/// <summary>
	/// A command to retrieve a single customer's data
	/// </summary>
	public class GetCustomerCommand : CommandBase<CustomerDto>
	{
		/// <summary>
		/// Customer's idetifier
		/// </summary>
		[Required]
		public int? Id { get; set; }
	}
}
