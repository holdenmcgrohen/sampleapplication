﻿using System.ComponentModel.DataAnnotations;
using SampleApplication.Core.Infrastructure.Commands;
using SampleApplication.DataContracts;

namespace SampleApplication.Core.Commands
{
	/// <summary>
	/// A command to create a new customer
	/// </summary>
	public class CreateCustomerCommand : CommandBase<CustomerDto>
	{
		/// <summary>
		/// Name
		/// </summary>
		[Required]
		[StringLength(500)]
		public string Name { get; set; }

		/// <summary>
		/// Email address
		/// </summary>
		[Required]
		[StringLength(500)]
		[EmailAddress]
		public string Email { get; set; }
	}
}
