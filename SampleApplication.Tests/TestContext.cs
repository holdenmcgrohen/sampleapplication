﻿using System;
using System.Collections.Generic;
using System.Linq;
using SampleApplication.Core.Domain;
using SampleApplication.Core.Infrastructure;
using SampleApplication.Core.Infrastructure.Storage;

namespace SampleApplication.Tests
{
	internal class TestContext
	{
		public TestContext()
		{
			this.Repository = new InMemoryRepository(false);
			ServiceLocator.Register<IRepository>(this.Repository);
		}

		public IRepository Repository { get; }

		public Order CreateOrder(Customer customer)
		{
			var order = new Order { Price = 1, Customer = customer };
			this.Repository.Save(order);
			return order;
		}

		public ICollection<Order> CreateOrders(Customer customer, int count)
		{
			return Enumerable.Range(1, count).Select(_ => this.CreateOrder(customer)).ToList();
		}

		public Customer CreateCustomer()
		{
			var customer = new Customer { Name = "TestUser", Email = $"{Guid.NewGuid()}@example.com" };
			this.Repository.Save(customer);
			return customer;
		}

		public Customer GetCustomer(int id)
		{
			return this.Repository.Query<Customer>().Where(c=>c.Id == id).SingleOrDefault();
		}

		public Customer GetCustomer(string email)
		{
			return this.Repository.Query<Customer>().Where(c => c.Email == email).SingleOrDefault();
		}

		public Order GetOrder(int id)
		{
			return this.Repository.Query<Order>().Where(c => c.Id == id).SingleOrDefault();
		}
	}
}
