﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SampleApplication.Web.Controllers;
using Shouldly;

namespace SampleApplication.Tests.Controllers
{
	/// <summary>
	/// Tests for <see cref="HomeController"/>
	/// </summary>
	[TestClass]
	public class HomeControllerTest
	{
		/// <summary>
		/// Ensures that web application controllers can be successfully instantiated, and action methods can be called
		/// </summary>
		[TestMethod]
		public void Index()
		{
			// Arrange
			HomeController controller = new HomeController();

			// Act
			string result = controller.Index();

			// Assert
			Assert.IsNotNull(result);
			result.ShouldBe("Sample API here");
		}
	}
}
