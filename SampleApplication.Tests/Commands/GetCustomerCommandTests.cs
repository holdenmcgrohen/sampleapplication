﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SampleApplication.Core.CommandHandlers;
using SampleApplication.Core.Commands;
using Shouldly;

namespace SampleApplication.Tests.Commands
{
	/// <summary>
	/// Tests for <see cref="GetCustomerCommand"/>
	/// </summary>
	[TestClass]
	public class GetCustomerCommandTests
	{
		[TestMethod]
		public void When_RequestedValidCustomerId_Should_ReturnCorrectData()
		{
			// Arrange
			var context = new TestContext();
			var customer = context.CreateCustomer();
			var orders = context.CreateOrders(customer, 3);
			var command = new GetCustomerCommand
			{
				Id = customer.Id
			};

			// Act
			var handler = new GetCustomerCommandHandler(context.Repository);
			var result = handler.Handle(command);

			// Assert
			result.Success.ShouldBeTrue();
			var customerDto = result.Data;
			customerDto.ShouldNotBeNull();
			customerDto.Id.ShouldBe(customer.Id);
			customerDto.Name.ShouldBe(customer.Name);
			customerDto.Email.ShouldBe(customer.Email);
			customerDto.Orders.ShouldNotBeNull();
			customerDto.Orders.Count.ShouldBe(orders.Count);
			customerDto.Orders.Select(o => o.Id).SequenceEqual(orders.Select(o => o.Id)).ShouldBeTrue();
		}

		[TestMethod]
		public void When_RequestedInvalidCustomerId_Should_FailWithCorrectMessage()
		{
			// Arrange
			var context = new TestContext();
			var command = new GetCustomerCommand
			{
				Id = 1
			};

			// Act
			var handler = new GetCustomerCommandHandler(context.Repository);
			var result = handler.Handle(command);

			// Assert
			result.Success.ShouldBeFalse();
			result.Errors.Single().ShouldBe("Customer not found");
		}

	}
}
