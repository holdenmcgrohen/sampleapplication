﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SampleApplication.Core.CommandHandlers;
using SampleApplication.Core.Commands;
using Shouldly;

namespace SampleApplication.Tests.Commands
{
	/// <summary>
	/// Tests for <see cref="GetCustomersCommand"/>
	/// </summary>
	[TestClass]
	public class GetCustomersCommandTests
	{
		[TestMethod]
		public void When_NoCustomersExist_Should_ReturnEmptyCollection()
		{
			// Arrange
			var context = new TestContext();
			var command = new GetCustomersCommand();

			// Acts
			var handler = new GetCustomersCommandHandler(context.Repository);
			var result = handler.Handle(command);

			// Assert
			result.Success.ShouldBeTrue();
			result.Data.Customers.Count.ShouldBe(0);
		}

		[TestMethod]
		public void When_OneCustomerExists_Should_ReturnCorrectData()
		{
			// Arrange
			var context = new TestContext();
			var customer = context.CreateCustomer();
			context.CreateOrder(customer);
			var command = new GetCustomersCommand();

			// Acts
			var handler = new GetCustomersCommandHandler(context.Repository);
			var result = handler.Handle(command);

			// Assert
			result.Success.ShouldBeTrue();
			var customerDataDto = result.Data;
			customerDataDto.ShouldNotBeNull();
			customerDataDto.Customers.ShouldNotBeNull();
			customerDataDto.Customers.Count.ShouldBe(1);
			var customerDto = customerDataDto.Customers.Single();
			customerDto.Id.ShouldBe(customer.Id);
			customerDto.Name.ShouldBe(customer.Name);
			customerDto.Email.ShouldBe(customer.Email);
			customerDto.Orders.ShouldBeNull();
		}
	}
}
