﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SampleApplication.Core.CommandHandlers;
using SampleApplication.Core.Commands;
using Shouldly;

namespace SampleApplication.Tests.Commands
{
	/// <summary>
	/// Tests for <see cref="CreateCustomerCommand"/>
	/// </summary>
	[TestClass]
	public class CreateCustomerCommandTests
	{
		[TestMethod]
		public void When_ParametersAreValid_Should_CreateCustomer()
		{
			// Arrange
			var context = new TestContext();
			var command = new CreateCustomerCommand
			{
				Email = "xx@xx.xx",
				Name = "Test Name"
			};

			// Act
			var handler = new CreateCustomerCommandHandler(context.Repository);
			var result = handler.Handle(command);

			// Assert
			result.Success.ShouldBeTrue();
			var customerDto = result.Data;
			customerDto.ShouldNotBeNull();
			customerDto.Id.ShouldBeGreaterThan(0);
			customerDto.Name.ShouldBe(command.Name);
			customerDto.Email.ShouldBe(command.Email);
			customerDto.Orders.ShouldNotBeNull();
			customerDto.Orders.Count.ShouldBe(0);
			context.GetCustomer(customerDto.Id).ShouldNotBeNull();
		}

		[TestMethod]
		public void When_InvalidEmailSpecified_Should_FailWithCorrectMessage()
		{
			// Arrange
			var context = new TestContext();
			var command = new CreateCustomerCommand
			{
				Email = "xx",
				Name = "Test Name"
			};

			// Act
			var handler = new CreateCustomerCommandHandler(context.Repository);
			var result = handler.Handle(command);

			// Assert
			result.Success.ShouldBeFalse();
			result.Errors.Single().ShouldBe("The Email field is not a valid e-mail address.");
			context.GetCustomer(command.Email).ShouldBeNull();
		}

		[TestMethod]
		public void When_LongStringsSpecified_Should_FailWithCorrectMessages()
		{
			// Arrange
			var context = new TestContext();
			string longString = new string('x', 1000);
			var command = new CreateCustomerCommand
			{
				Email = $"{longString}@example.com",
				Name = longString
			};

			// Act
			var handler = new CreateCustomerCommandHandler(context.Repository);
			var result = handler.Handle(command);

			// Assert
			result.Success.ShouldBeFalse();
			result.Errors.Count.ShouldBe(2);
			result.Errors.ShouldContain("The field Name must be a string with a maximum length of 500.");
			result.Errors.ShouldContain("The field Email must be a string with a maximum length of 500.");
		}
	}
}
