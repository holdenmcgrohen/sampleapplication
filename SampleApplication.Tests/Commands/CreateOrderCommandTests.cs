﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SampleApplication.Core.CommandHandlers;
using SampleApplication.Core.Commands;
using Shouldly;

namespace SampleApplication.Tests.Commands
{
	/// <summary>
	/// Tests for <see cref="CreateOrderCommand"/>
	/// </summary>
	[TestClass]
	public class CreateOrderCommandTests
	{
		[TestMethod]
		public void When_ParametersAreValid_Should_CreateOrder()
		{
			// Arrange
			var context = new TestContext();
			var customer = context.CreateCustomer();
			var command = new CreateOrderCommand
			{
				CustomerId = customer.Id,
				Price = 1
			};

			// Act
			var handler = new CreateOrderCommandHandler(context.Repository);
			var result = handler.Handle(command);

			// Assert
			result.Success.ShouldBeTrue();
			var orderDto = result.Data;
			orderDto.ShouldNotBeNull();
			orderDto.Id.ShouldBeGreaterThan(0);
			orderDto.Price.ShouldBe(command.Price.Value);
			context.GetOrder(orderDto.Id).ShouldNotBeNull();
		}

		[TestMethod]
		public void When_InvalidCustomerIdSpecified_Should_FailWithCorrectMessage()
		{
			// Arrange
			var context = new TestContext();
			var command = new CreateOrderCommand
			{
				CustomerId = 1,
				Price = 1
			};

			// Act
			var handler = new CreateOrderCommandHandler(context.Repository);
			var result = handler.Handle(command);

			// Assert
			result.Success.ShouldBeFalse();
			result.Errors.Single().ShouldBe("Customer not found");
		}

		[TestMethod]
		public void When_InvalidPriceSpecified_Should_FailWithCorrectMessage()
		{
			// Arrange
			var context = new TestContext();
			var customer = context.CreateCustomer();
			var command = new CreateOrderCommand
			{
				CustomerId = customer.Id,
				Price = -1
			};

			// Act
			var handler = new CreateOrderCommandHandler(context.Repository);
			var result = handler.Handle(command);

			// Assert
			result.Success.ShouldBeFalse();
			result.Errors.Single().ShouldBe("The field Price must be between 0 and 100000.");
		}
	}
}
