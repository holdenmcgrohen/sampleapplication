﻿using System;

namespace SampleApplication.DataContracts
{
	/// <summary>
	/// Customer's order
	/// </summary>
	public class OrderDto
	{		
		/// <summary>
		/// Unique identifier
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Creation date
		/// </summary>
		public DateTime CreationDate { get; set; }

		/// <summary>
		/// Total price
		/// </summary>
		public decimal Price { get; set; }
	}
}