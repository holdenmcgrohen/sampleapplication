﻿using System.Collections.Generic;

namespace SampleApplication.DataContracts
{
	/// <summary>
	/// Product representation used for data transfer
	/// </summary>
	public class CustomerDto
	{
		/// <summary>
		/// Unique identifier
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Name of the customer
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Email of the customer
		/// </summary>
		public string Email { get; set; }

		/// <summary>
		/// All customer's orders
		/// </summary>
		public ICollection<OrderDto> Orders { get; set; }
	}
}
