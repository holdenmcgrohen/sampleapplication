﻿using System.Collections.Generic;

namespace SampleApplication.DataContracts
{
	/// <summary>
	/// Products data wrapper used for data transfer
	/// </summary>
	public class CustomersDataWrapperDto
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="CustomersDataWrapperDto"/> class.
		/// </summary>
		public CustomersDataWrapperDto()
		{
			this.Customers = new List<CustomerDto>();
		}

		/// <summary>
		/// Products that match the query criteria
		/// </summary>
		public ICollection<CustomerDto> Customers { get; set; }
	}
}
