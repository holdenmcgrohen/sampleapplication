﻿using System.Collections.Generic;

namespace SampleApplication.DataContracts
{
	/// <summary>
	/// Represents the standard API response
	/// </summary>
	/// <typeparam name="TResultData">The type of the data that the response contains</typeparam>
	public class ApiResponse<TResultData>
	{
		/// <summary>
		/// Indicates whether the request was executed successfully
		/// </summary>
		public bool Success { get; set; }

		/// <summary>
		/// Error message
		/// </summary>
		public ICollection<string> Errors { get; set; }

		/// <summary>
		/// Request-specific data that describes the request processing result
		/// </summary>
		public TResultData Data { get; set; }
	}
}
