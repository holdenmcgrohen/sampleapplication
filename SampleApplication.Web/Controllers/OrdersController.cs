﻿using System.Net.Http;
using System.Web.Http;
using SampleApplication.Core.Commands;
using SampleApplication.DataContracts;

namespace SampleApplication.Web.Controllers
{
	/// <summary>
	/// Controller for order-related operations
	/// </summary>
	[RoutePrefix("api/orders")]
	public class OrdersController : ApiControllerBase
    {
		/// <summary>
		/// Adds an order to an existing customer
		/// </summary>
		[HttpPost]
		public HttpResponseMessage CreateOrder([FromBody] CreateOrderCommand command)
		{
			return this.ProcessCommand<CreateOrderCommand, OrderDto>(command);
		}
	}
}
