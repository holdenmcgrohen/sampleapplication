﻿using System.Net.Http;
using System.Web.Http;
using SampleApplication.Core.Commands;
using SampleApplication.DataContracts;

namespace SampleApplication.Web.Controllers
{
	/// <summary>
	/// Controller for customer-related operations
	/// </summary>
	[RoutePrefix("api/customers")]
	public class CustomersController : ApiControllerBase
	{
		/// <summary>
		/// Gets all available customers
		/// </summary>
		[HttpGet]
		public HttpResponseMessage GetCustomers(GetCustomersCommand command)
		{
			return this.ProcessCommand<GetCustomersCommand, CustomersDataWrapperDto>(command);
		}

		/// <summary>
		/// Gets all available customers
		/// </summary>
		[HttpGet]
		[Route("{id:int}")]
		public HttpResponseMessage GetCustomer([FromUri]GetCustomerCommand command)
		{
			return this.ProcessCommand<GetCustomerCommand, CustomerDto>(command);
		}

		/// <summary>
		/// Creates a new customer
		/// </summary>
		[HttpPost]
		public HttpResponseMessage CreateCustomer([FromBody] CreateCustomerCommand command)
		{
			return this.ProcessCommand<CreateCustomerCommand, CustomerDto>(command);
		}
	}
}