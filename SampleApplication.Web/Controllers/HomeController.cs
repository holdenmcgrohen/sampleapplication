﻿using System.Web.Mvc;

namespace SampleApplication.Web.Controllers
{
	/// <summary>
	/// Default controller
	/// </summary>
	public class HomeController : Controller
	{
		/// <summary>
		/// Default action
		/// </summary>
		public string Index()
		{
			return "Sample API here";
		}
	}
}
