﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using SampleApplication.Core.Infrastructure.Commands;

namespace SampleApplication.Web.Controllers
{
	/// <summary>
	/// Base controller capable of processing commands
	/// </summary>
	public abstract class ApiControllerBase : ApiController
	{
		protected HttpResponseMessage ProcessCommand<TCommand, TResultData>(TCommand command)
			where TCommand : CommandBase<TResultData>, new()
		{
			if (command == null)
			{
				command = new TCommand();
			}

			var result = CommandProcessor.Process<TCommand, TResultData>(command);
			return this.Request.CreateResponse(result.Success ? HttpStatusCode.OK : HttpStatusCode.BadRequest, result);
		}
	}
}