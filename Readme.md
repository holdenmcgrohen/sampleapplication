﻿# .NET technical assignment

## The task
Create a .NET REST API serving the domain of Customers and Orders.

## The solution

**Technologies and frameworks used:**  
1) ASP.NET WebAPI — provides the means to handle HTTP requests and easily serialize/deserialize data  
2) SQLite database - as a lightweight and portable way of storing data  
3) NHibernate - ORM for accessing DB data  
4) Castle Windsor — a IoC container for decoupling components and resolving component dependencies and settings at runtime  

**Startup web project:** `SampleApplication.Web`

**Unit test project:** `SampleApplication.Tests`

**API Endpoints:**  
`GET /api/customers` - get all customers (without orders)  
`GET /api/customers/{id}` - get a customer with the specified ID (with orders)  
`POST api/customers` - create a new customer  
`POST api/orders` - add an order to an existing customer  